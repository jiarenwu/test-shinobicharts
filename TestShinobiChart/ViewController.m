//
//  ViewController.m
//  TestShinobiChart
//
//  Created by Richard Wu on 3/02/2016.
//  Copyright © 2016 WP. All rights reserved.
//

#import "ViewController.h"
#import <ShinobiCharts/ShinobiCharts.h>
#import "MainGraphDrawDataDetails.h"


@interface ViewController ()<SChartDatasource>
@property (weak, nonatomic) IBOutlet ShinobiChart *mainChart;

@end

@implementation ViewController {
    NSMutableArray *mainGraphDrawDataArray;
}

- (NSDate *)dateFromString:(NSString *)dateString {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"yyyy-MM-dd";
    return [format dateFromString:dateString];
}
#pragma mark - Configure charts
- (void)configureCharts {
    //Main Chart
    self.mainChart.autoresizingMask =  ~UIViewAutoresizingNone;
    
    
    SChartDiscontinuousDateTimeAxis *xAxis = [[SChartDiscontinuousDateTimeAxis alloc] init];
    
    SChartRepeatedTimePeriod* weekends = [[SChartRepeatedTimePeriod alloc] initWithStart:[self dateFromString:@"2001-01-06"]
                                                                               andLength:[SChartDateFrequency dateFrequencyWithDay:2]
                                                                            andFrequency:[SChartDateFrequency dateFrequencyWithWeek:1]];
    [xAxis addExcludedRepeatedTimePeriod:weekends];
    xAxis.labelFormatString = @"dd MMM yy";
    self.mainChart.xAxis = xAxis;
    
    
    
    
    SChartNumberAxis *yAxis = [[SChartNumberAxis alloc] init];
    yAxis.title = NSLocalizedString(@"PRICE",nil);
    yAxis.rangePaddingHigh = @0.1;
    yAxis.rangePaddingLow = @0.1;
    self.mainChart.yAxis = yAxis;
    
    SChartNumberAxis *rightY = [SChartNumberAxis new];
    rightY.axisPosition = SChartAxisPositionReverse; //use the non-default position
    
    rightY.style.majorTickStyle.showLabels = NO;
    rightY.style.majorTickStyle.showTicks = NO;
    [self.mainChart addYAxis:rightY];
    
    // enable gestures
    xAxis.enableGesturePanning = YES;
    xAxis.enableGestureZooming = YES;
    xAxis.allowPanningOutOfDefaultRange = YES;
    xAxis.allowPanningOutOfMaxRange = NO;
    
    self.mainChart.datasource = self;
    
    self.mainChart.autoCalculateAxisRanges = YES;
    
    
    self.mainChart.crosshair.enableCrosshairLines = YES;

    self.mainChart.gestureDoubleTapEnabled = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureCharts];
    mainGraphDrawDataArray = [[NSMutableArray alloc] init];
    
    [self addDataSet];    
}

- (BOOL) isDateInWeekend:(NSDate *)chosenDate {
    //Calculate the provided date
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"c"; // day number, like 7 for saturday
    
    NSString *dayOfWeek = [formatter stringFromDate:chosenDate];
    return [dayOfWeek isEqualToString:@"7"] || [dayOfWeek isEqualToString:@"1"];
}

- (void)excludeDateFromXAxises:(NSDate *)nextDate {
    
    for (SChartDiscontinuousDateTimeAxis *axis in self.mainChart.allXAxes) {
        [axis addExcludedTimePeriod:[[SChartTimePeriod alloc] initWithStart:nextDate andLength:[SChartDateFrequency dateFrequencyWithDay:1] ]];
    }
}

- (void)removeAddedExcludePeriodFromXAxises {
    
    for (SChartDiscontinuousDateTimeAxis *axis in self.mainChart.allXAxes) {
        NSArray *allExcludePeriods = [axis.excludedTimePeriods copy];
        for (SChartTimePeriod *period in allExcludePeriods) {
            [axis removeExcludedTimePeriod:period];
        }
    }

}
- (void)updateCharts_xAxises {
    
    [self removeAddedExcludePeriodFromXAxises];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"yyyy-MM-dd";
    NSCalendar *curCalendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:1];
    NSDate *nextDate = nil;
    
    NSDate *lastDate = [self dateFromString:@"2016-01-01"];
    
    BOOL found = NO;
    
    MainGraphDrawDataDetails *details = (MainGraphDrawDataDetails *)[mainGraphDrawDataArray firstObject];
    nextDate = details.transactionDate;
    
    while ([nextDate compare:lastDate] == NSOrderedAscending) {
        found = NO;
        for (MainGraphDrawDataDetails *details  in mainGraphDrawDataArray) {
            if ([details.transactionDate compare:nextDate] == NSOrderedSame) {
                found = YES;
                break;
            }
        }
        
        
        if (!found && ![self isDateInWeekend:nextDate]) {
            [self excludeDateFromXAxises:nextDate];
        }
        nextDate = [curCalendar dateByAddingComponents:comps toDate:nextDate options:0];
    }
    
}
- (void) addDataSet {
    //2015-12-31,5.62,5.66,5.59,5.61,15555100,5.61#2015-12-30,5.56,5.61,5.55,5.61,18237500,5.61#2015-12-29,5.47,5.53,5.43,5.53,14908900,5.53#2015-12-24,5.48,5.48,5.44,5.46,5309700,5.46#2015-12-23,5.48,5.48,5.42,5.42,11662000,5.42#2015-12-22,5.49,5.50,5.41,5.43,16455100,5.4
    MainGraphDrawDataDetails *details0 = [[MainGraphDrawDataDetails alloc] initWithTransactionDate:[self dateFromString:@"2015-12-22"] Open:5.49 HighestPrice:5.50 LowestPrice:5.41 ClosePrice:5.43 Volume:16455100];
    [mainGraphDrawDataArray addObject:details0];
    MainGraphDrawDataDetails *details1 = [[MainGraphDrawDataDetails alloc] initWithTransactionDate:[self dateFromString:@"2015-12-23"] Open:5.48 HighestPrice:5.48 LowestPrice:5.42 ClosePrice:5.42 Volume:111662000];
    [mainGraphDrawDataArray addObject:details1];
    
    MainGraphDrawDataDetails *details2 = [[MainGraphDrawDataDetails alloc] initWithTransactionDate:[self dateFromString:@"2015-12-24"] Open:5.48 HighestPrice:5.48 LowestPrice:5.44 ClosePrice:5.46 Volume:5309700];
    [mainGraphDrawDataArray addObject:details2];
    
    MainGraphDrawDataDetails *details3 = [[MainGraphDrawDataDetails alloc] initWithTransactionDate:[self dateFromString:@"2015-12-29"] Open:5.47 HighestPrice:5.53 LowestPrice:5.43 ClosePrice:5.53 Volume:114908900];
    [mainGraphDrawDataArray addObject:details3];
    
    MainGraphDrawDataDetails *details4 = [[MainGraphDrawDataDetails alloc] initWithTransactionDate:[self dateFromString:@"2015-12-30"] Open:5.56 HighestPrice:5.61 LowestPrice:5.55 ClosePrice:5.61 Volume:18237500];
    [mainGraphDrawDataArray addObject:details4];
    
    MainGraphDrawDataDetails *details5 = [[MainGraphDrawDataDetails alloc] initWithTransactionDate:[self dateFromString:@"2015-12-31"] Open:5.62 HighestPrice:5.66 LowestPrice:5.59 ClosePrice:5.61 Volume:16455100];
    [mainGraphDrawDataArray addObject:details5];
    
    [self updateCharts_xAxises];
    [self.mainChart reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - SChartDatasource methods

- (SChartAxis *)sChart:(ShinobiChart *)chart yAxisForSeriesAtIndex:(int)index {
    
    if (index == 0) {
        return [chart.secondaryYAxes objectAtIndex:0];
    }
    
    
    return chart.yAxis;
}


- (NSInteger)numberOfSeriesInSChart:(ShinobiChart *)chart {
    return 2;
}


-(SChartSeries *)sChart:(ShinobiChart *)chart seriesAtIndex:(NSInteger)index {

    switch (index) {
        case 0:
        {
            SChartColumnSeries* columnSeries = [[SChartColumnSeries alloc] init];
            
            columnSeries.selectionMode = SChartSelectionPoint;//
            columnSeries.toggleSelection = YES;
            columnSeries.title = @"Volume";
            columnSeries.showInLegend = NO;
            columnSeries.crosshairEnabled = YES;
            
            
            columnSeries.style.showAreaWithGradient = NO;
            columnSeries.style.lineColor = [UIColor colorWithRed:150.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:0.6];
            columnSeries.style.areaColor = [UIColor colorWithRed:150.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:0.6];// [UIColor grayColor];
            return columnSeries;
        }
            break;
        case 1: {
            SChartCandlestickSeries* candleStickSeries = [[SChartCandlestickSeries alloc] init];
            
            candleStickSeries.selectionMode = SChartSelectionPoint;
            candleStickSeries.togglePointSelection = YES;// NO;
            candleStickSeries.title = @"Test";//kCandleGraphPlot;
            candleStickSeries.showInLegend = NO;
            candleStickSeries.crosshairEnabled = YES;
            
            
            return candleStickSeries;
            break;
        }
            
        default:
            break;
    }
    
    return nil;
}

- (NSInteger)sChart:(ShinobiChart *)chart numberOfDataPointsForSeriesAtIndex:(NSInteger)seriesIndex {
    
    return [mainGraphDrawDataArray count];
}
- (SChartDataPoint *)nilDataPoint {
    static SChartDataPoint *nilPoint = nil;
    if (! nilPoint) {
        nilPoint = [SChartDataPoint new];
        nilPoint.xValue = [NSDate date];//@([[NSDate date] timeIntervalSince1970]);
        nilPoint.yValue = [NSNumber numberWithFloat:0.0];
    }
    
    return nilPoint;
}
- (id<SChartData>)sChart:(ShinobiChart *)chart dataPointAtIndex:(NSInteger)dataIndex forSeriesAtIndex:(NSInteger)seriesIndex {
    

    MainGraphDrawDataDetails *mainDrawDataDetails = (MainGraphDrawDataDetails *)[mainGraphDrawDataArray objectAtIndex:dataIndex];
    
    NSDate *xValue = mainDrawDataDetails.transactionDate;
    
    switch (seriesIndex) {
        case 0:
        {
            MainGraphDrawDataDetails *mainDrawDataDetails = (MainGraphDrawDataDetails *)[mainGraphDrawDataArray objectAtIndex:dataIndex];
            SChartDataPoint *datapoint = [SChartDataPoint new];
            datapoint.xValue = xValue;
            
            datapoint.yValue = [NSNumber numberWithLongLong:mainDrawDataDetails.volume];
            
            return datapoint;
        }
            
            break;
        case 1:
        {
            SChartMultiYDataPoint* datapoint = [SChartMultiYDataPoint new];
            datapoint.xValue = xValue;
            NSDictionary* yValues = @{SChartCandlestickKeyOpen: [NSNumber numberWithDouble:mainDrawDataDetails.openPrice],
                                      SChartCandlestickKeyHigh: [NSNumber numberWithDouble:mainDrawDataDetails.highestPrice],
                                      SChartCandlestickKeyLow: [NSNumber numberWithDouble:mainDrawDataDetails.lowestPrice],
                                      SChartCandlestickKeyClose: [NSNumber numberWithDouble:mainDrawDataDetails.closePrice]};
            datapoint.yValues = [NSMutableDictionary dictionaryWithDictionary:yValues];
            
            return datapoint;
        }
            
            break;
            
            
            break;
        default:
            break;
    }
    return nil;
}


@end
