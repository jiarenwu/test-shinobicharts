//
//  MainGraphDrawDataDetails.h
//  TestShinobiChart
//
//  Created by Richard Wu on 3/02/2016.
//  Copyright © 2016 WP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainGraphDrawDataDetails : NSObject
@property (nonatomic, assign)long long volume;
@property (nonatomic, assign)float openPrice;
@property (nonatomic, assign)float highestPrice;
@property (nonatomic, assign)float lowestPrice;
@property (nonatomic, assign)float closePrice;
@property (nonatomic, strong)NSDate *transactionDate;

-(id)initWithTransactionDate:(NSDate *)transactionDate Open:(float)openPrice HighestPrice:(float)highestPrice LowestPrice:(float)lowestPrice ClosePrice:(float)closePrice Volume:(long long)volume;
@end
