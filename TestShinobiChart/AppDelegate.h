//
//  AppDelegate.h
//  TestShinobiChart
//
//  Created by Richard Wu on 3/02/2016.
//  Copyright © 2016 WP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

