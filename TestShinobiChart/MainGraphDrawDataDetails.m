//
//  MainGraphDrawDataDetails.m
//  TestShinobiChart
//
//  Created by Richard Wu on 3/02/2016.
//  Copyright © 2016 WP. All rights reserved.
//

#import "MainGraphDrawDataDetails.h"

@implementation MainGraphDrawDataDetails
-(id)initWithTransactionDate:(NSDate *)transactionDate Open:(float)openPrice HighestPrice:(float)highestPrice LowestPrice:(float)lowestPrice ClosePrice:(float)closePrice Volume:(long long)volume  {
    if ((self = [super init])) {

        self.transactionDate = transactionDate;
        self.openPrice = openPrice;
        self.highestPrice = highestPrice;
        self.lowestPrice = lowestPrice;
        self.closePrice = closePrice;
        self.volume = volume;
    }
    return self;
}
@end
